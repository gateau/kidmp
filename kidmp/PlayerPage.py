# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.phonon import *

from PyKDE4.kdecore import *
from PyKDE4.kdeui import *

SEEK_DELTA = 30 * 1000

AUTOSAVE_INTERVAL = 10 * 1000

HUD_ANIM_DURATION = 300
AUTOHIDE_POSITION_BAR_INTERVAL = 2 * 1000 + 2 * HUD_ANIM_DURATION

from ButtonBar import ButtonBar
from PositionBar import PositionBar
from PositionDb import PositionDb

# Debug helper
def phononStateString(state):
    lst = "PlayingState", "PausedState", "BufferingState", "StoppedState"
    dct = dict((eval("Phonon." + x), x) for x in lst)
    return dct.get(state, "Unknown state (%d)" % state)

class PlayerPage(QWidget):
    backRequested = pyqtSignal()
    snapshoted = pyqtSignal(QString, QPixmap)

    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.path = None
        self.pendingSeek = None

        self.setMouseTracking(True)
        self.mediaObject = Phonon.MediaObject(self)
        self.restriction = None
        self.positionDb = PositionDb()

        self.videoWidget = Phonon.VideoWidget(self)
        Phonon.createPath(self.mediaObject, self.videoWidget)
        self.audioOutput = Phonon.AudioOutput(Phonon.VideoCategory, self)
        Phonon.createPath(self.mediaObject, self.audioOutput)

        layout = QVBoxLayout(self)
        layout.setMargin(0)
        layout.addWidget(self.videoWidget)

        self.autoSaveTimer = QTimer(self)
        self.autoSaveTimer.setInterval(AUTOSAVE_INTERVAL)
        QObject.connect(self.autoSaveTimer, SIGNAL("timeout()"), self.savePosition)

        self.setupActions()

        self.setupPositionBar()
        self.setupButtonBar()
        self.setupHudWidget()

        QObject.connect(self.mediaObject, SIGNAL("stateChanged(Phonon::State, Phonon::State)"), self.slotStateChanged)
        self.mediaObject.finished.connect(self.slotFinished)

    def setupActions(self):
        self.backAction = QAction(self)
        self.backAction.setText(i18n("Back"))
        self.backAction.setIcon(KIcon("go-home"))
        self.backAction.triggered.connect(self.slotBackActionTriggered)

        self.resumeAction = QAction(self)
        self.resumeAction.setText(i18nc("Resume playback", "Resume"))
        self.resumeAction.setIcon(KIcon("media-playback-start"))
        QObject.connect(self.resumeAction, SIGNAL("triggered(bool)"), self.resume)

        self.goToStartAction = QAction(self)
        self.goToStartAction.setText(i18n("Restart"))
        self.goToStartAction.setIcon(KIcon("go-previous"))
        QObject.connect(self.goToStartAction, SIGNAL("triggered(bool)"), self.goToStart)

        self.snapshotAction = QAction(self)
        self.snapshotAction.setText(i18n("Photo"))
        self.snapshotAction.setIcon(KIcon("camera-photo"))
        self.snapshotAction.setShortcut(Qt.Key_P)
        QObject.connect(self.snapshotAction, SIGNAL("triggered(bool)"), self.snapshot)

        self.addAction(self.backAction)
        self.addAction(self.snapshotAction)

    def setupPositionBar(self):
        self.positionBar = PositionBar(self.mediaObject)
        self.positionBarAutoHideTimer = QTimer(self)
        self.positionBarAutoHideTimer.setSingleShot(True)
        self.positionBarAutoHideTimer.setInterval(AUTOHIDE_POSITION_BAR_INTERVAL)
        self.positionBarAutoHideTimer.timeout.connect(self.hideHudWidget)

    def setupButtonBar(self):
        self.buttonBar = ButtonBar()
        self.buttonBar.addAction(self.backAction)
        self.buttonBar.addStretch()
        self.resumeButton = self.buttonBar.addAction(self.resumeAction)
        self.buttonBar.addAction(self.goToStartAction)
        self.buttonBar.addStretch()
        self.buttonBar.addAction(self.snapshotAction)

    def setupHudWidget(self):
        self.hudWidget = QWidget(self)
        self.hudWidget.hide()
        layout = QVBoxLayout(self.hudWidget)
        layout.setMargin(0)
        layout.setSpacing(0)
        layout.addWidget(self.positionBar)
        layout.addWidget(self.buttonBar)
        self.hudWidget.setFixedHeight(self.hudWidget.sizeHint().height())

        self.hudAnim = QPropertyAnimation(self.hudWidget, "pos", self)
        self.hudAnim.setDuration(HUD_ANIM_DURATION)
        self.hudAnim.setEasingCurve(QEasingCurve.OutQuad)

    def resizeEvent(self, event):
        QWidget.resizeEvent(self, event)
        self.hudWidget.setFixedWidth(self.width())
        self.hudWidget.move(0, self.height())

    def play(self, path):
        self.restriction = KNotificationRestrictions(KNotificationRestrictions.ScreenSaver)
        self.path = path
        url = QUrl.fromLocalFile(path)
        source = Phonon.MediaSource(url)
        self.mediaObject.setCurrentSource(source)
        self.pendingSeek = self.positionDb.read(url)
        self.mediaObject.play()
        self.hideHudWidget()

    def goBack(self):
        self.restriction = None
        if self.mediaObject.state != Phonon.StoppedState:
            self.mediaObject.stop()
        self.autoSaveTimer.stop()
        self.backRequested.emit()

    def savePosition(self):
        url = self.mediaObject.currentSource().url()
        position = self.mediaObject.currentTime()
        self.positionDb.write(url, position)

    def resetSavedPosition(self):
        url = self.mediaObject.currentSource().url()
        self.positionDb.write(url, 0)

    def showHudWidget(self, full=False):
        self.buttonBar.setEnabled(full)
        if full:
            height = self.hudWidget.height()
            self.positionBarAutoHideTimer.stop()
            self.resumeButton.setFocus()
        else:
            height = self.positionBar.height()
            self.positionBarAutoHideTimer.start()

        self.hudWidget.raise_()
        self.hudWidget.show()
        self.slideHudWidgetTo(self.height() - height)

    def hideHudWidget(self):
        self.slideHudWidgetTo(self.height())
        self.setFocus()

    def slideHudWidgetTo(self, y):
        currentPos = self.hudWidget.pos()
        newPos = QPoint(0, y)
        if newPos == currentPos:
            return
        self.hudAnim.setStartValue(currentPos)
        self.hudAnim.setEndValue(newPos)
        self.hudAnim.start()

    def pause(self):
        self.mediaObject.pause()
        self.showHudWidget(full=True)

    def resume(self):
        self.hideHudWidget()
        self.mediaObject.play()

    def goToStart(self):
        self.hideHudWidget()
        self.mediaObject.seek(0)
        self.mediaObject.play()

    def forward(self):
        self.showHudWidget(full=False)
        self.mediaObject.seek(self.mediaObject.currentTime() + SEEK_DELTA)

    def rewind(self):
        self.showHudWidget(full=False)
        self.mediaObject.seek(self.mediaObject.currentTime() - SEEK_DELTA)

    def snapshot(self):
        hudY = self.hudWidget.y()
        if hudY < self.height():
            self.hudWidget.hide()
            def callback():
                self.doSnapshot()
                self.hudWidget.show()
            # Waiting a bit to ensure the widget is hidden
            QTimer.singleShot(100, callback)
        else:
            self.doSnapshot()

    def doSnapshot(self):
        # self.videoWidget.snapshot() returns an empty image, so we fake it
        pix = QPixmap.grabWindow(self.videoWidget.winId())

        # Calling sizeHint() gets the real size of the video
        size = self.videoWidget.sizeHint()

        # Remove the black bars
        ratio = min(self.width() / float(size.width()), self.height() / float(size.height()))
        scaledSize = size * ratio

        pix = pix.copy( \
            (self.width() - scaledSize.width()) / 2, \
            (self.height() - scaledSize.height()) / 2, \
            scaledSize.width(), scaledSize.height() \
            )

        self.snapshoted.emit(self.path, pix)

    def slotBackActionTriggered(self):
        self.savePosition()
        self.goBack()

    def slotFinished(self):
        self.resetSavedPosition()
        self.goBack()

    def slotStateChanged(self, state, oldState):
        if self.pendingSeek is not None:
            if state in (Phonon.PlayingState, Phonon.PausedState, Phonon.BufferingState):
                self.mediaObject.seek(self.pendingSeek)
                self.pendingSeek = None

    def keyPressEvent(self, event):
        super(PlayerPage, self).keyPressEvent(event)
        key = event.key()
        if key in (Qt.Key_Space, Qt.Key_Return):
            self.pause()
        elif key == Qt.Key_Left:
            self.rewind()
        elif key == Qt.Key_Right:
            self.forward()
