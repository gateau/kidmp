#!/usr/bin/env python
# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from PyKDE4.kdecore import *
from PyKDE4.kdeui import *

from MainWindow import MainWindow

VERSION = "0.4.0"

STRUT = 40

def main():
    aboutData = KAboutData(
        "kidmp", # appName
        "", # catalogName
        ki18n("Kid Media Player"), # programName
        VERSION)
    aboutData.setLicense(KAboutData.License_GPL_V2)
    aboutData.setShortDescription(ki18n("Kid Media Player"))
    aboutData.setCopyrightStatement(ki18n("(c) 2011-2012 Aurélien Gâteau"))
    aboutData.setProgramIconName("kde")

    KCmdLineArgs.init(sys.argv, aboutData)

    options = KCmdLineOptions()
    options.add("nofullscreen", ki18n("Start with a normal window. Useful for debugging."))
    KCmdLineArgs.addCmdLineOptions(options)

    app = KApplication()

    args = KCmdLineArgs.parsedArgs()

    pal = QPalette(QColor("#333"))
    pal.setColor(QPalette.Window, Qt.black)
    pal.setColor(QPalette.Highlight, QColor("#2163AD"))
    app.setPalette(pal)
    app.setGlobalStrut(QSize(STRUT, STRUT))
    window = MainWindow()

    if args.isSet("fullscreen"):
        window.showFullScreen()
    else:
        window.show()
    app.exec_()
    return 0


if __name__=="__main__":
    sys.exit(main())
# vi: ts=4 sw=4 et
