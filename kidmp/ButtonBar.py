# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyKDE4.kdeui import *

class ButtonBarButton(QToolButton):
    """
    A button with no tooltips and more visible rendering of focused state
    """
    def __init__(self, parent = None):
        QToolButton.__init__(self, parent)
        self.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.setIconSize(QSize(48, 48))

    def event(self, event):
        if event.type() == QEvent.ToolTip:
            event.ignore()
            return True
        elif event.type() == QEvent.MouseButtonRelease and event.button() == Qt.RightButton:
            if self.isEnabled():
                self.defaultAction().trigger()
            event.ignore()
            return True
        else:
            return QToolButton.event(self, event)

    def paintEvent(self, event):
        if self.hasFocus():
            painter = QStylePainter(self)
            opt = QStyleOptionToolButton()
            self.initStyleOption(opt)

            # Alter the background color to make focused state more visible
            pal = self.palette()
            bgColor = KColorUtils.mix(pal.color(QPalette.Button), pal.color(QPalette.Highlight), 0.7)
            pal.setColor(QPalette.Window, bgColor)
            pal.setColor(QPalette.Button, bgColor)
            pal.setColor(QPalette.ButtonText, pal.color(QPalette.HighlightedText))
            opt.palette = pal

            painter.drawComplexControl(QStyle.CC_ToolButton, opt)
        else:
            QToolButton.paintEvent(self, event)

class ButtonBar(QFrame):
    def __init__(self, parent = None):
        QFrame.__init__(self, parent)
        self.setFrameStyle(QFrame.NoFrame)
        self.setAutoFillBackground(True)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        layout = QHBoxLayout(self)
        layout.setMargin(0)

    def addStretch(self):
        self.layout().addStretch()

    def addSpacing(self, spacing=12):
        self.layout().addSpacing(spacing)

    def addAction(self, action):
        QFrame.addAction(self, action)
        button = ButtonBarButton(self)
        button.setDefaultAction(action)
        self.layout().addWidget(button)
        return button

    def addWidget(self, widget):
        self.layout().addWidget(widget)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(self.rect(), QColor.fromHsvF(0, 0, 0.1))

    def keyPressEvent(self, event):
        QFrame.keyPressEvent(self, event)
        if event.key() == Qt.Key_Return:
            widget = self.focusWidget()
            if widget:
                widget.animateClick()
                event.accept()
