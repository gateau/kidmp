# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from PyKDE4.kdecore import *
from PyKDE4.kdeui import *

from BrowserPage import BrowserPage
from PlayerPage import PlayerPage
from ThumbnailProvider import ThumbnailProvider

CURSOR_AUTOHIDE_DELAY= 2000

class MainWindow(KMainWindow):
    def __init__(self):
        KMainWindow.__init__(self)

        self.browserPage = BrowserPage()
        self.playerPage = PlayerPage()

        self.stack = QStackedWidget()
        self.setCentralWidget(self.stack)

        self.stack.addWidget(self.browserPage)
        self.stack.addWidget(self.playerPage)

        self.browserPage.activated.connect(self.play)
        self.playerPage.backRequested.connect(self.goToBrowserPage)
        self.playerPage.snapshoted.connect(self.updateThumbnail)

        self.setupCursor()

    def setupCursor(self):
        # Make sure cursor does not trigger any mouse over effect
        QCursor.setPos(0, 0)
        self.cursorTimer = QTimer(self)
        self.cursorTimer.setInterval(CURSOR_AUTOHIDE_DELAY)
        self.cursorTimer.setSingleShot(True)
        self.cursorTimer.timeout.connect(self.hideCursor)
        self.hideCursor()

    def event(self, event):
        if event.type() == QEvent.HoverMove:
            if not event.oldPos().isNull() and event.pos() != event.oldPos():
                self.setCursor(Qt.ArrowCursor)
                self.cursorTimer.start()
        return super(MainWindow, self).event(event)

    def hideCursor(self):
        self.setCursor(Qt.BlankCursor)

    def play(self, path):
        self.stack.setCurrentWidget(self.playerPage)
        self.playerPage.play(path)

    def goToBrowserPage(self):
        self.stack.setCurrentWidget(self.browserPage)

    def updateThumbnail(self, path, pix):
        ThumbnailProvider.saveThumbnail(path, pix)
        self.browserPage.refreshThumbnail(path)
