# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *

PATH_ROLE = Qt.UserRole + 1
NORMAL_THUMBNAIL_ROLE = Qt.UserRole + 2
MIRRORED_THUMBNAIL_ROLE = Qt.UserRole + 3

class ThumbnailInfo(object):
    def __init__(self, videoDir, name):
        self.videoDir = videoDir
        self.name = name
        self.revision = 0

    def path(self):
        return os.path.join(self.videoDir, self.name)

    def url(self, prefix):
        return "image://thumbnail/%s/%s@revision=%d" % (prefix, self.path(), self.revision)

    def __cmp__(self, other):
        return cmp(self.name, other.name)

    def update(self):
        self.revision += 1

class ThumbnailModel(QAbstractListModel):
    def __init__(self, parent):
        super(ThumbnailModel, self).__init__(parent)
        self.videoDir = u""
        self.entries = []

        roleNames = self.roleNames()
        roleNames[PATH_ROLE] = "path"
        roleNames[NORMAL_THUMBNAIL_ROLE] = "normalThumbnail"
        roleNames[MIRRORED_THUMBNAIL_ROLE] = "mirroredThumbnail"
        self.setRoleNames(roleNames)

    def setVideoDir(self, videoDir):
        def isVideo(entry):
            path = os.path.join(self.videoDir, entry)
            return os.path.isfile(path) and entry[0] != u"."

        self.videoDir = unicode(videoDir)
        entries = [ThumbnailInfo(self.videoDir, x) for x in os.listdir(self.videoDir) if isVideo(x)]
        entries.sort()
        self.beginResetModel()
        self.entries = []
        self.endResetModel()
        if entries:
            self.beginInsertRows(QModelIndex(), 0, len(entries) - 1)
            self.entries = entries
            self.endInsertRows()

    def updateThumbnail(self, path):
        row = self._rowForPath(path)
        self.entries[row].update()
        index = self.createIndex(row, 0)
        self.dataChanged.emit(index, index)

    def data(self, index, role = Qt.DisplayRole):
        if index.row() < 0 or index.row() >= len(self.entries):
            return QModelIndex()
        if role == Qt.DisplayRole:
            return self.entries[index.row()].name
        elif role == PATH_ROLE:
            return self.entries[index.row()].path()
        elif role == NORMAL_THUMBNAIL_ROLE:
            return self.entries[index.row()].url("normal")
        elif role == MIRRORED_THUMBNAIL_ROLE:
            return self.entries[index.row()].url("mirrored")
        else:
            raise Exception(u"Invalid role %d" % role)

    def rowCount(self, index):
        if index.isValid():
            return 0
        else:
            return len(self.entries)

    def pathForIndex(self, index):
        assert index.row() >= 0
        assert index.row() < len(self.entries)
        return self.entries[index.row()].path()

    def _rowForPath(self, path):
        path = unicode(path)
        for row, entry in enumerate(self.entries):
            if entry.path() == path:
                return row
        raise Exception(u"No entry for path '%s'" % path)
