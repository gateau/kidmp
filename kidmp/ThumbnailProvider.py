# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtDeclarative import *

from PyKDE4.kdeui import *

THUMBNAIL_DIR = ".covers"

def mirroredPix(src):
    transform = QTransform()
    transform.scale(1, -0.618)
    dst = src.transformed(transform)

    gradient = QLinearGradient(0, 0, 0, dst.height())
    gradient.setColorAt(0, QColor.fromRgbF(0, 0, 0, 0.618))
    gradient.setColorAt(0.6, Qt.black)
    painter = QPainter(dst)
    painter.fillRect(dst.rect(), gradient)
    return dst

def iconPathForPath(path):
    aDir, name = os.path.split(path)
    return os.path.join(aDir, THUMBNAIL_DIR, name + ".jpg")

class ThumbnailProvider(QDeclarativeImageProvider):
    THUMBNAIL_SIZE = QSize(320, 240)

    def __init__(self):
        QDeclarativeImageProvider.__init__(self, QDeclarativeImageProvider.Pixmap)

    def requestPixmap(self, imageId, size, requestedSize):
        thumbnailType, path = unicode(imageId).split("/", 1)
        path = path.split("@", 1)[0]

        assert thumbnailType in ("normal", "mirrored")

        iconPath = iconPathForPath(path)
        pix = QPixmap(iconPath)
        if pix.isNull():
            size = max(self.THUMBNAIL_SIZE.width(), self.THUMBNAIL_SIZE.height())
            pix = KIconLoader.global_().loadIcon("video", KIconLoader.Desktop, size)

        if thumbnailType == "mirrored":
            pix = mirroredPix(pix)

        return pix

    @staticmethod
    def saveThumbnail(path, pix):
        iconPath = iconPathForPath(unicode(path))
        iconDir = os.path.dirname(iconPath)
        if not os.path.exists(iconDir):
            os.makedirs(iconDir)
        pix = pix.scaled(ThumbnailProvider.THUMBNAIL_SIZE, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        pix.save(iconPath)
