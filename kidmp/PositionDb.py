# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyKDE4.kdecore import *

def _key(url):
    return unicode(url.toString())

class PositionDb(object):
    def __init__(self):
        self.db = dict()
        self.config = KConfig()
        self.group = KConfigGroup(self.config, "positions")

    def read(self, url):
        value = self.group.readEntry(_key(url), 0)
        position, ok = value.toLongLong()
        if ok:
            return position
        else:
            return 0

    def write(self, url, position):
        self.group.writeEntry(_key(url), position)
        self.group.sync()
