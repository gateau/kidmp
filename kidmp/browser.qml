/*
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 1.0

Item {
    property int currentRow: list.currentIndex
    signal selected()
    ListView {
        id: list
        anchors.fill: parent
        model: videoListModel
        orientation: ListView.Horizontal
        focus: true

        preferredHighlightBegin: width / 2 - 160
        preferredHighlightEnd: width / 2 + 160
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 500

        delegate: Item {
            id: me
            width: normalImage.sourceSize.width
            height: ListView.view.height

            // Goes from 0 to 1
            property real normX:
                ListView.view.width == 0 ? 0 // prevent division by 0
                : (x - ListView.view.contentX + width / 2) / ListView.view.width

            property real itemScale: Math.sin(Math.PI * normX)
            opacity: 0.2 + 0.8 * itemScale
            transform: [
                Rotation {
                    origin.x: width / 2
                    origin.y: baseY
                    axis { x: 0; y: 1; z: 0 }
                    angle: (normX - 0.5) * -120
                }
                ,
                Scale {
                    origin.x: width / 2
                    origin.y: baseY
                    xScale: 0.7 + 0.3 * itemScale
                    yScale: xScale
                }
            ]

            property int baseY: height * 0.618
            Image {
                id: normalImage
                y: baseY - sourceSize.height
                anchors.horizontalCenter: parent.horizontalCenter
                source: normalThumbnail
            }
            Image {
                y: baseY
                anchors.horizontalCenter: parent.horizontalCenter
                source: mirroredThumbnail
            }
        }

        Keys.onSpacePressed: {
            selected();
        }
        Keys.onReturnPressed: {
            selected();
        }
    }
}
