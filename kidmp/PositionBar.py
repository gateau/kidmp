# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.phonon import *

from PyKDE4.kdecore import *

class PositionBar(QLabel):
    def __init__(self, mediaObject, parent=None):
        QLabel.__init__(self, parent)
        self.mediaObject = mediaObject

        self.setAutoFillBackground(True)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

        self.mediaObject.tick.connect(self.updateContent)
        self.mediaObject.totalTimeChanged.connect(self.updateContent)

    def updateContent(self):
        def div(x, y):
            return x / y, x % y

        time = self.mediaObject.remainingTime() / 1000

        minutes, seconds = div(time, 60)
        hours, minutes = div(minutes, 60)
        if hours > 0:
            txt = i18nc("Time left: %1 is hours, %2 is minutes", "%1h%2mn", hours, minutes)
        elif minutes > 0:
            txt = i18nc("Less than one hour left, show number of minutes", "%1mn", minutes)
        else:
            txt = i18nc("Less than one minute left, show number of seconds", "%1s", seconds)
        self.setText(txt)
        # Force an update in case text did not change
        self.update()

    def paintEvent(self, event):
        total = self.mediaObject.totalTime()

        painter = QPainter(self)
        painter.fillRect(self.rect(), QColor.fromHsvF(0, 0, 0.05))

        if total > 0:
            percent = self.mediaObject.currentTime() / float(total)
            margin = 2
            rect = QRect(margin, margin + 1, int((self.width() - 2 * margin) * percent), self.height() - 2 * margin - 1)
            painter.setRenderHint(QPainter.Antialiasing)
            rect.translate(-.5, -.5)
            painter.setBrush(self.palette().color(QPalette.Highlight))
            painter.setPen(Qt.NoPen)
            radius = 3
            painter.drawRoundedRect(rect, radius, radius)
            painter.setRenderHint(QPainter.Antialiasing, False)

        painter.setPen(QColor.fromHsvF(0, 0, 0.1))
        painter.drawLine(0, 0, self.width(), 0)
        painter.end()
        QLabel.paintEvent(self, event)
