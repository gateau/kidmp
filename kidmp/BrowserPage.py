# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtDeclarative import *

from PyKDE4.kdecore import *
from PyKDE4.kdeui import *
from PyKDE4.kio import *

from ThumbnailProvider import ThumbnailProvider
from ThumbnailModel import ThumbnailModel
from ButtonBar import ButtonBar

MAX_HISTORY_SIZE = 8

HPADDING = 4

BASEDIR = os.path.dirname(__file__)

class BrowserPage(QWidget):
    activated = pyqtSignal(QString)

    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.setAutoFillBackground(True)
        self.starting = True

        self.model = ThumbnailModel(self)

        self.setupView()

        self.setupButtonBar()

        self.loadHistory()
        if self.history.isEmpty():
            self.selectDir()
        else:
            self.setCurrentDir(self.history[0])

        layout = QVBoxLayout(self)
        layout.setMargin(0)
        layout.setSpacing(0)
        layout.addWidget(self.view)
        layout.addWidget(self.buttonBar)

    def setupView(self):
        self.view = QDeclarativeView(self)
        self.view.setResizeMode(QDeclarativeView.SizeRootObjectToView)

        self.thumbnailProvider = ThumbnailProvider()
        self.view.engine().addImageProvider("thumbnail", self.thumbnailProvider)

        self.view.rootContext().setContextProperty("videoListModel", self.model)
        self.view.rootContext().setContextProperty("currentDir", "")

        url = QUrl(os.path.join(BASEDIR, "browser.qml"))
        self.view.setSource(url)

        self.currentRowProperty = QDeclarativeProperty(self.view.rootObject(), "currentRow")
        self.currentRowProperty.connectNotifySignal(self.updateTitleLabel)
        self.view.rootObject().selected.connect(self.emitActivated)

    def setupButtonBar(self):
        self.titleLabel = QLabel()
        font = self.font()
        font.setPixelSize(24)
        self.titleLabel.setFont(font)
        self.titleLabel.setAlignment(Qt.AlignCenter)

        self.menuAction = QAction(self)
        self.menuAction.setIcon(KIcon("configure"))
        QObject.connect(self.menuAction, SIGNAL("triggered(bool)"), self.showMenu)

        self.buttonBar = ButtonBar(self)
        self.buttonBar.addWidget(self.titleLabel)
        button = self.buttonBar.addAction(self.menuAction)

        # Helps keeping the title label centered, even if there is a button on the right
        menuButtonWidth = button.sizeHint().width()
        self.titleLabel.setContentsMargins(menuButtonWidth, 0, 0, 0)

    def showMenu(self):
        menu = QMenu()
        menu.addAction(KStandardAction.open(self.selectDir, self))
        historyActions = []
        if not self.history.isEmpty():
            menu.addSeparator()
            for entry in self.history:
                action = QAction(self)
                action.setText(entry)
                menu.addAction(action)
                historyActions.append(action)
        menu.addSeparator()

        # About action
        helpMenu = KHelpMenu(self, KCmdLineArgs.aboutData(), False)
        menu.addAction(KStandardAction.aboutApp(helpMenu.aboutApplication, None))

        menu.addAction(KStandardAction.quit(qApp.closeAllWindows, self))

        size = menu.sizeHint()
        pos = self.mapToGlobal(self.buttonBar.geometry().topRight())
        pos.setX(pos.x() - size.width())
        pos.setY(pos.y() - size.height())
        action = menu.exec_(pos)

        try:
            idx = historyActions.index(action)
        except ValueError:
            return
        self.setCurrentDir(self.history[idx])


    def setCurrentDir(self, currentDir):
        self.model.setVideoDir(currentDir)
        self.addToHistory(currentDir)

    def selectDir(self):
        currentDir = self.model.videoDir
        url = KDirSelectDialog.selectDirectory(KUrl.fromPath(currentDir),
            True, self,
            i18n("Select Video Folder")
            )
        if not url.isEmpty():
            self.setCurrentDir(url.path())

    def emitActivated(self):
        index = self._currentIndex()
        path = self.model.pathForIndex(index)
        self.activated.emit(path)

    def loadHistory(self):
        self.config = KConfig()
        self.historyConfigGroup = KConfigGroup(self.config, "general")
        value = self.historyConfigGroup.readEntry("history", QStringList())
        self.history =  value.toStringList()

    def addToHistory(self, entry):
        if self.history.contains(entry):
            self.history.removeAll(entry)
        self.history.prepend(entry)
        self.history = self.history[:MAX_HISTORY_SIZE]
        self.historyConfigGroup.writeEntry("history", self.history)

    def refreshThumbnail(self, path):
        self.model.updateThumbnail(path)

    def updateTitleLabel(self):
        index = self._currentIndex()
        text = u""
        if index.isValid():
            text = index.data().toString()
            text = text.section(".", 0, -2).replace("_", " ").replace(".", " ")
        self.titleLabel.setText(text)

    def _currentIndex(self):
        row = self.currentRowProperty.read().toInt()[0]
        return self.model.index(row, 0)
