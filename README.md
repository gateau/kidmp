# Introduction

KidMP is a media player primarily designed for kids. This application can be
used while travelling to replace a portable DVD player.

It has been designed to work well on a laptop in an environment where using the
touchpad is difficult (think of a 5 year-old holding the laptop on his laps in
the car). As such it is primarily keyboard-based.

Features which make KidMP a kid-friendly player:

- Large, customizable thumbnails
- No need to be able to read
- Fully usable with only 3 keys: left arrow, right arrow and space (or return)
- Automatically resume playing at last position
- Start in fullscreen by default, difficult to quit by accident

# Usage

Once setup (see the "Getting Started" section), the minimum you need to teach
your kid is this:

- Select movies with the left and right arrows
- Press Space to start playback
- Press Space again to pause playback. A toolbar appears at the bottom.
- When paused, you can either:
    - press Space again to resume playback
    - use the left and right arrows to highlight the "Home" button, then press
      Space to go back to the movie list

# Getting Started

When KidMP is started for the first time, all you get is a black screen and a
small config button in the bottom-right corner.

- Click the config button and select "Open" in the menu which appears.
- Pick a folder filled with kid-friendly movies.

KidMP should show one icon for each movie. It is now time to define custom
thumbnails. For each movie:

1. Start playback.
2. Use the arrow keys to seek.
3. Press Space to pause.
4. Click the "Photo" button.
5. Click the "Home" button to go back to the movie list.

Note: You may not have to do this by yourself: in my experience kids are able
to change the thumbnails themselves, and usually enjoy doing it!

# Additional Playback Shortcuts

Your kid does not need to know these shortcuts but they can be handy:

- P: Takes a photo.
- Left and right arrows: Seeks back and forward 30 seconds.
