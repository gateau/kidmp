# encoding: utf-8
"""
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

Based on code from python-distutils-extra
<https://launchpad.net/python-distutils-extra>:

Copyright 2007, 2008 Sebastian Heinlein
Copyright 2009 Canonical Ltd.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import distutils
import distutils.command.build
import glob
import os

def setup(**kwargs):
    """
    A version of distutils.core.setup() which takes care of compiling .po files
    into .mo files and installing them in the right place.

    It assumes the following:

    - all .po files are in the po/ dir.
    - the domain name is the project name.

    It does not require the presence of a POTFILES.in file.

    It does *not* automatically generate a .pot file or update the .po files at
    build time. These two tasks can be done with the update_po command:

        python setup.py update_po

    # Why not use distutils-extra?

    This code is heavily inspired from distutils-extra
    (https://launchpad.net/python-distutils-extra)

    KDE applications do not use _() to mark translated strings: they use
    i18n(), i18nc(), i18np() or i18ncp(). This means we need to pass special
    options to xgettext to generate the .pot file.

    I wanted to use distutils-extra but I could not find how to either pass
    options to xgettext or disable .pot file generation all together. I am also
    not fond of requiring a POTFILES.in file. Therefore I resorted to extract
    the code responsible for compiling the .po and installing them.
    """
    cmdclass = {
        "build": kbuild,
        "build_i18n": build_i18n,
        "update_po": update_po,
    }
    if "cmdclass" in kwargs:
        # FIXME: Make it possible for the user to override the "build" command?
        kwargs["cmdclass"].update(cmdclass)
    else:
        kwargs["cmdclass"] = cmdclass
    distutils.core.setup(**kwargs)

class build_i18n(distutils.cmd.Command):

    description = "generate .mo files from .po files and add them to the list of files to be installed"

    def initialize_options(self):
        self.domain = None
        self.po_dir = None

    def finalize_options(self):
        if self.domain is None:
            self.domain = self.distribution.metadata.name
        if self.po_dir is None:
            self.po_dir = "po"

    def run(self):
        """
        Generate mo files and add them to the to be installed files
        """
        if not os.path.isdir(self.po_dir):
            return

        data_files = self.distribution.data_files
        if data_files is None:
            # in case not data_files are defined in setup.py
            self.distribution.data_files = data_files = []

        max_po_mtime = 0
        for po_file in glob.glob("%s/*.po" % self.po_dir):
            lang = os.path.basename(po_file[:-3])
            mo_dir =  os.path.join("build", "mo", lang, "LC_MESSAGES")
            mo_file = os.path.join(mo_dir, "%s.mo" % self.domain)
            if not os.path.exists(mo_dir):
                os.makedirs(mo_dir)
            cmd = ["msgfmt", po_file, "-o", mo_file]
            po_mtime = os.path.getmtime(po_file)
            mo_mtime = os.path.exists(mo_file) and os.path.getmtime(mo_file) or 0
            if po_mtime > max_po_mtime:
                max_po_mtime = po_mtime
            if po_mtime > mo_mtime:
                self.spawn(cmd)

            targetpath = os.path.join("share/locale", lang, "LC_MESSAGES")
            data_files.append((targetpath, (mo_file,)))

class update_po(distutils.cmd.Command):
    description = "update .pot and .po files"

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def _list_source_files(self, exts):
        lst = []
        IGNORED_DIRS = [".git", ".svn", ".bzr", "build", "dist"]
        for root, dirs, files in os.walk("."):
            for ignored_dir in IGNORED_DIRS:
                if ignored_dir in dirs:
                    dirs.remove(ignored_dir)
            src = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in exts]
            lst.extend(src)
        return lst

    def run(self):
        name = self.distribution.metadata.name
        po_dir = "po"

        # Generate .pot file
        pot_file = "%s/%s.pot" % (po_dir, name)
        keywords = [
            "i18n",
            "i18nc:1c,2",
            "i18np:1,2",
            "i18ncp:1c,2,3",
            "ki18n"]

        src_files = self._list_source_files([".py"])
        cmd = ["xgettext"] \
            + ["-k" + x for x in keywords] \
            + ["--language=Python", "-o", pot_file] \
            + src_files
        self.spawn(cmd)

        # Update .po files
        for po in glob.glob("%s/*.po" % po_dir):
            cmd = ["msgmerge", "--backup=none", "--update", po, pot_file]
            self.spawn(cmd)

class kbuild(distutils.command.build.build):
    """
    Make sure the build_i18n command is called when building
    """
    def finalize_options(self):
        distutils.command.build.build.finalize_options(self)
        self.sub_commands.append(("build_i18n", lambda x: True))
