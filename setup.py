#!/usr/bin/env python
# encoding: utf-8
"""
KidMP: a kid friendly media player
Copyright 2012 Aurélien Gâteau <agateau@kde.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from kdistutils import setup

from kidmp import kidmp

NAME = "kidmp"

DESCRIPTION = "Kid Friendly Media Player"

setup(name=NAME,
    version=kidmp.VERSION,
    description=DESCRIPTION,
    author="Aurélien Gâteau",
    author_email="agateau@kde.org",
    license="GPLv3 or later",
    platforms=["any"],
    requires=["PyQt4", "PyKDE4"],
    url="http://github.com/agateau/kidmp",
    packages=["kidmp"],
    package_data={"kidmp": ["*.qml"]},
    data_files=[
        ("share/applications/kde4", ["desktop/kidmp.desktop"]),
        ("share/doc/kidmp", ["LICENSE", "README.md"]),
    ],
    scripts=["bin/kidmp"],
    )
